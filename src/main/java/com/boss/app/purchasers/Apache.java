package com.boss.app.purchasers;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import org.apache.pdfbox.exceptions.CryptographyException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.encryption.BadSecurityHandlerException;
import org.apache.pdfbox.pdmodel.encryption.StandardDecryptionMaterial;
import org.apache.pdfbox.util.PDFTextStripper;

import com.boss.app.storage.ExcelData;
import com.boss.app.util.Utilities;

public class Apache {

	public static ArrayList<ExcelData> parseApachePDF(File file, String purchaser) throws IOException {

		try (PDDocument document = PDDocument.load(file)) {
			// try (PDDocument document = PDDocument.load(new
			// File("C:\\Users\\Shawn\\jeremy\\jeremy\\src\\main\\java\\fileUpload\\test2.pdf"))){

			PDFTextStripper stripper = new PDFTextStripper();
			Map<String, Integer> numPages = document.getPageMap();
			System.out.println("number of pages = " + numPages.size());
			/*
			 * insert Loop for number of pages, so you can removed the unnessary fields then
			 * the logic below should work for the APACHE
			 */
			String text = stripper.getText(document);
			int startIndex;
			int endIndex;
			String toBeReplaced;

			// System.out.println(text);

			for (int i = 0; i < numPages.size() - 1; i++) {
				// first page
				if (i == 0) {

					startIndex = text.indexOf("OWNER REMITTANCE ADVICE");
					endIndex = text.indexOf("OWNER INTEREST");

					toBeReplaced = text.substring(startIndex, endIndex + 14);
					text = text.replace(toBeReplaced, "");

					startIndex = text.indexOf("PRODUCT CODES:");
					endIndex = text.indexOf("OWNER INTEREST");
					toBeReplaced = text.substring(startIndex, endIndex + 14);
					text = text.replace(toBeReplaced, "");
				}
				// Last Page
				else if (i == numPages.size() - 1) {
					startIndex = text.indexOf("OWNER REMITTANCE ADVICE");
					endIndex = text.indexOf("OWNER TOTAL");
					toBeReplaced = text.substring(startIndex, endIndex);
					text = text.replace(toBeReplaced, "");

				} else {
					startIndex = text.indexOf("OWNER REMITTANCE ADVICE");
					endIndex = text.indexOf("OWNER INTEREST");

					toBeReplaced = text.substring(startIndex, endIndex + 14);
					text = text.replace(toBeReplaced, "");

				}

			}

			System.out.println("Text size: " + text.length() + " characters:");
			System.out.println(text);

			return createListOfobjects(text, purchaser);

		}
	}

	public static ArrayList<ExcelData> createListOfobjects(String text, String purchaser) throws IOException {
		ArrayList<ExcelData> dataList = new ArrayList<ExcelData>();
		String wellId = "";

		String[] split = text.split("\\r?\\n");
		System.out.println(split.length);
		for (int i = 0; i < split.length - 1; i++) {
			if (split[i].equals("")) {
				// do nothing empty string
			} else {

				if (split[i].startsWith("DOI")) {
					// set well id so it can be used for all the objects

					String[] values = split[i].split("\\s+");
					if (values[1].substring(7).equals("LEBON")) {
						wellId = "01597601";
					} else {
						wellId = split[i].substring(split[i].length() - 8);
						System.out.println(wellId);
					}

				} else if (split[i].contains("PROPERTY TOTAL") || split[i].contains("OWNER TOTAL")) {
					// skip because we do not use any of these values
				}

				else {
					// this is where most of the data will be set in the object

					ExcelData data = Utilities.createDataObject();
					data.setPurchaser(purchaser);
					data.setPropertyOne(wellId);

					// property file eventually
					data.setIntcode("RI");

					String[] values = split[i].split("\\s+");

					if (values[0].startsWith("1")) {
						data.setProdcode("OIL");
					} else if (values[0].startsWith("2")) {
						data.setProdcode("GAS");
					} else if (values[0].startsWith("3")) {
						data.setProdcode("CND");
					} else if (values[0].startsWith("4")) {
						data.setProdcode("PP");
					}

					data.setRundate(values[10].substring(values[10].length() - 7));
					data.setBblMcf(values[1]);
					data.setOwnerGross(values[6]);
					data.setTaxes(values[7]);
					data.setOther(values[8]);
					data.setNet(values[12]);

					dataList.add(data);
				}

			}
		}

		System.out.println("Number of well objects = " + dataList.size());
		return dataList;
	}

	// this was just my test for landscape
	public static void parseLandscapePDF(File file)
			throws IOException, BadSecurityHandlerException, CryptographyException {
		try (PDDocument document = PDDocument.load(file)) {

			StandardDecryptionMaterial dm = new StandardDecryptionMaterial("");
			document.openProtection(dm);
			PDFTextStripper stripper = new PDFTextStripper();
			String text = stripper.getText(document);
			System.out.println("Text size: " + text.length() + " characters:");
			System.out.println(text);
		}
	}

}
