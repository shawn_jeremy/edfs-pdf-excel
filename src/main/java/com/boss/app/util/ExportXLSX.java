package com.boss.app.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.boss.app.storage.ExcelData;

/**
 * Takes data from ExcelData Pojo and creates an XLSX WorkSheet
 */
public class ExportXLSX {

	public static File export(ArrayList<ExcelData> dataList) throws IOException {

		String[] columns = { "Purchaser Code", "Division Order Property#", "Division Order Property#2",
				"Division Order Property#3", "Interest Code", "Product Code", "Run Date", "Comment", "100% BBL/MCF",
				"Owner Gross", "Taxes", "Other", "OK Tax CDX", "Net" };

		Workbook workbook = new XSSFWorkbook(); // new HSSFWorkbook() for generating `.xls` file

		// Create a Sheet
		Sheet sheet = workbook.createSheet("Report");

		// Create a Row
		Row headerRow = sheet.createRow(0);

		// Create cells
		for (int i = 0; i < columns.length; i++) {
			Cell cell = headerRow.createCell(i);
			cell.setCellValue(columns[i]);
		}

		// Create Other rows and cells with employees data
		int rowNum = 1;
		for (ExcelData data : dataList) {
			Row row = sheet.createRow(rowNum++);

			row.createCell(0).setCellValue(data.getPurchaser());
			row.createCell(1).setCellValue(data.getPropertyOne());
			row.createCell(2).setCellValue(data.getPropertyTwo());
			row.createCell(3).setCellValue(data.getPropertyThree());
			row.createCell(4).setCellValue(data.getIntcode());
			row.createCell(5).setCellValue(data.getProdcode());
			row.createCell(6).setCellValue(data.getRundate());
			row.createCell(7).setCellValue(data.getComment());
			row.createCell(8).setCellValue(data.getBblMcf());
			row.createCell(9).setCellValue(data.getOwnerGross());
			row.createCell(10).setCellValue(data.getTaxes());
			row.createCell(11).setCellValue(data.getOther());
			row.createCell(12).setCellValue(data.getStateTax());
			row.createCell(13).setCellValue(data.getNet());

		}

		// Resize all columns to fit the content size
		for (int i = 0; i < columns.length; i++) {
			sheet.autoSizeColumn(i);
		}

		// Write the output to a file
		File file = new File("ReportExport.xlsx");
		FileOutputStream fileOut = new FileOutputStream(file);
		workbook.write(fileOut);

		fileOut.close();

		// Closing the workbook
		workbook.close();

		return file;
	}

}
