package com.boss.app.util;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

//@SuppressWarnings("serial")
public class JFrameDesign extends JFrame {
	public JFrameDesign() {
		createView();
	}

	JPanel panel = new JPanel();
	private String purchaser = null;
	private String email = null;
	private String filePath = null;

	public String getPurchaser() {
		return purchaser;
	}

	public void setPurchaser(String purchaser) {
		this.purchaser = purchaser;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	/*
	 * Creates the view for the UI
	 */
	private void createView() {
		panel.setLayout(new BorderLayout());
		getContentPane().add(panel);
		setTitle("Checkstub Converter tool");

		setSize(350, 200);
		setMinimumSize(new Dimension(350, 175));
		setLocationRelativeTo(null);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// North Pane
		JPanel panelNorth = new JPanel();
		panel.add(panelNorth, BorderLayout.NORTH);
		// Label for Purchaser Input
		JLabel label2 = new JLabel();
		label2.setText("Enter Purchaser");
		panelNorth.add(label2);

		// Text Box for Purchaser Input
		JTextField purchaserText = new JTextField();
		purchaserText.setPreferredSize(new Dimension(200, 30));
		panelNorth.add(purchaserText);
		//

		// Center Pane
		JPanel panelCenter = new JPanel();
		panel.add(panelCenter, BorderLayout.CENTER);
		// Label for Email Input
		JLabel label = new JLabel();
		label.setText("Enter Email: ");
		panelCenter.add(label);

		// Text Box for Email Input
		JTextField emailText = new JTextField();
		emailText.setPreferredSize(new Dimension(200, 30));
		panelCenter.add(emailText);
		//

		// South Pane
		JPanel panelSouth = new JPanel();
		panel.add(panelSouth, BorderLayout.SOUTH);

		// Choose File to Upload
		JButton openChooser = new JButton("Choose File to Upload");
		openChooser.setBounds(100, 100, 140, 40);
		openChooser.addActionListener(new chooseFileActionListener());
		panelSouth.add(openChooser);

		// Complete Button
		JButton button = new JButton("Convert");
		button.setBounds(100, 100, 140, 40);
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				purchaser = purchaserText.getText();
				email = emailText.getText();
				System.out.println("Purchaser: " + purchaser);
				System.out.println("Email: " + email);
				System.out.println("File Path: " + filePath);
				setPurchaser(purchaser);
				setEmail(email);
				setFilePath(filePath);
				try {
					PDFParser.companySelector(email, purchaser, filePath);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		panelSouth.add(button);
		//
	}

	private class chooseFileActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			JButton open = new JButton();
			JFileChooser chooser = new JFileChooser();
			FileFilter pdf = new FileNameExtensionFilter("Pdf file(.pdf)", "pdf");
			chooser.setFileFilter(pdf);
			chooser.setCurrentDirectory(new File("."));
			chooser.setDialogTitle("Choose PDF File");
			chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);

			if (chooser.showOpenDialog(open) == JFileChooser.APPROVE_OPTION) {
			}
			filePath = chooser.getSelectedFile().getAbsolutePath();
			System.out.println("File Path: " + filePath);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return purchaser + " " + filePath + " " + email;
	}

}
