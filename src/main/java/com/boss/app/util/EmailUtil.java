
package com.boss.app.util;

import java.io.File;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 * Send email to users after xlsx file has been created
 */
public class EmailUtil {

	/**
	 * @param export - file from ExportXLSX
	 * @param email  - recipients email
	 */
	public static void send(File export, String email) {

		final String username = "shawn.jeremy.test@gmail.com";
		final String password = "123!@#qweQWE";

		// setting smtp properties
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");
		props.put("mail.smtp.user", username);
		props.put("mail.smtp.password", password);
		props.put("mail.smtp.ssl.enable", "true");
		System.out.println("Before authentication");

		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});
		System.out.println("After authentication");
		try {

			Message message = new MimeMessage(session);
			// Senders email address
			message.setFrom(new InternetAddress("shawn.jeremy.test@gmail.com"));
			// Recipients email address
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));

			// add the Subject of email
			message.setSubject("Report From Boss APP");
			message.setText("");

			MimeBodyPart messageBodyPart = new MimeBodyPart();

			Multipart multipart = new MimeMultipart();

			// add the body
			messageBodyPart = new MimeBodyPart();

			// attach the file
			String fileName = "Export.xlsx";
			DataSource source = new FileDataSource(export);
			messageBodyPart.setDataHandler(new DataHandler(source));
			messageBodyPart.setFileName(fileName);
			multipart.addBodyPart(messageBodyPart);

			message.setContent(multipart);

			// Send Message
			System.out.println("Sending");
			Transport.send(message);

			System.out.println("Done");

		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}
}
