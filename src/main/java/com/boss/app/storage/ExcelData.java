/**
 *  Pojo to store data to send to ExportXLSX to write excel
 */
package com.boss.app.storage;

public class ExcelData {

	private String purchaser;
	private String propertyOne;
	private String propertyTwo;
	private String propertyThree;
	private String intcode;
	private String prodcode;
	private String rundate;
	private String comment;
	private String bblMcf;
	private String ownerGross;
	private String taxes;
	private String other;
	private String stateTax;
	private String net;

	public ExcelData() {
		super();
	}

	/**
	 * @param purchaser
	 * @param propertyOne
	 * @param propertyTwo
	 * @param propertyThree
	 * @param intcode
	 * @param prodcode
	 * @param rundate
	 * @param comment
	 * @param bblMcf
	 * @param ownerGross
	 * @param taxes
	 * @param other
	 * @param stateTax
	 * @param net
	 */
	public ExcelData(String purchaser, String propertyOne, String propertyTwo, String propertyThree, String intcode,
			String prodcode, String rundate, String comment, String bblMcf, String ownerGross, String taxes,
			String other, String stateTax, String net) {
		super();
		this.purchaser = purchaser;
		this.propertyOne = propertyOne;
		this.propertyTwo = propertyTwo;
		this.propertyThree = propertyThree;
		this.intcode = intcode;
		this.prodcode = prodcode;
		this.rundate = rundate;
		this.comment = comment;
		this.bblMcf = bblMcf;
		this.ownerGross = ownerGross;
		this.taxes = taxes;
		this.other = other;
		this.stateTax = stateTax;
		this.net = net;
	}

	public String getPurchaser() {
		return purchaser;
	}

	public void setPurchaser(String purchaser) {
		this.purchaser = purchaser;
	}

	public String getPropertyOne() {
		return propertyOne;
	}

	public void setPropertyOne(String propertyOne) {
		this.propertyOne = propertyOne;
	}

	public String getPropertyTwo() {
		return propertyTwo;
	}

	public void setPropertyTwo(String propertyTwo) {
		this.propertyTwo = propertyTwo;
	}

	public String getPropertyThree() {
		return propertyThree;
	}

	public void setPropertyThree(String propertyThree) {
		this.propertyThree = propertyThree;
	}

	public String getIntcode() {
		return intcode;
	}

	public void setIntcode(String intcode) {
		this.intcode = intcode;
	}

	public String getProdcode() {
		return prodcode;
	}

	public void setProdcode(String prodcode) {
		this.prodcode = prodcode;
	}

	public String getRundate() {
		return rundate;
	}

	public void setRundate(String rundate) {
		this.rundate = rundate;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getBblMcf() {
		return bblMcf;
	}

	public void setBblMcf(String bblMcf) {
		this.bblMcf = bblMcf;
	}

	public String getOwnerGross() {
		return ownerGross;
	}

	public void setOwnerGross(String ownerGross) {
		this.ownerGross = ownerGross;
	}

	public String getTaxes() {
		return taxes;
	}

	public void setTaxes(String taxes) {
		this.taxes = taxes;
	}

	public String getOther() {
		return other;
	}

	public void setOther(String other) {
		this.other = other;
	}

	public String getStateTax() {
		return stateTax;
	}

	public void setStateTax(String stateTax) {
		this.stateTax = stateTax;
	}

	public String getNet() {
		return net;
	}

	public void setNet(String net) {
		this.net = net;
	}

	@Override
	public String toString() {
		return "excelData [purchaser=" + purchaser + ", propertyOne=" + propertyOne + ", propertyTwo=" + propertyTwo
				+ ", propertyThree=" + propertyThree + ", intcode=" + intcode + ", prodcode=" + prodcode + ", rundate="
				+ rundate + ", comment=" + comment + ", bblMcf=" + bblMcf + ", ownerGross=" + ownerGross + ", taxes="
				+ taxes + ", other=" + other + ", stateTax=" + stateTax + ", net=" + net + "]";
	}

}